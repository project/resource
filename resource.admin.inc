<?php
/**
 * resource.admin.inc - Resource admin page callbacks.
 *
 * @author Arto Bendiken <http://bendiken.net/>
 * @copyright Copyright (c) 2007-2008 Arto Bendiken. All rights reserved.
 * @license GPL <http://creativecommons.org/licenses/GPL/2.0/>
 * @package resource.module
 */

//////////////////////////////////////////////////////////////////////////////
// Resource mass import

function resource_admin_import(&$form_state, $edit = array('uris' => 'http://')) {
  $edit = (object)$edit;
  $form = array();

  $form['import'] = array('#type' => 'fieldset', '#title' => t('Import resources from URIs'));
  $form['import']['uris'] = array('#type' => 'textarea', '#title' => t('URIs'), '#default_value' => $edit->uris, '#rows' => 10, '#required' => TRUE, '#description' => t('Enter one URI per line.'));

  $form['import']['submit'] = array('#type' => 'submit', '#value' => t('Import'));
  return $form;
}

function resource_admin_import_validate($form, &$form_state) {
  extract($form_state['values'], EXTR_SKIP | EXTR_REFS);

  $result = array();
  foreach (array_filter(explode("\n", str_replace("\r", '', $uris)), 'strlen') as $uri) {
    if (!rdf_is_valid_uri($uri)) {
      form_set_error('uris', t('%uri is not a valid URI.', array('%uri' => $uri)));
    }
    else {
      $result[] = $uri;
    }
  }
  $uris = $result;
}

function resource_admin_import_submit($form, &$form_state) {
  extract($form_state['values'], EXTR_SKIP | EXTR_REFS);

  $counter = 0;
  foreach ($uris as $uri) {
    // TODO
    $node = (object)array('type' => 'resource', 'resource_uri' => $uri);
    require_once drupal_get_path('module', 'node') . '/node.pages.inc';
    node_object_prepare($node);
    node_save($node);
    $counter++;
  }

  drupal_set_message(format_plural($counter, '1 resource imported.', '@count resources imported.'));
  $form_state['redirect'] = 'admin/content/node';
}
